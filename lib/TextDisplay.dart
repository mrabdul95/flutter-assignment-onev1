import 'package:flutter/material.dart';
class TextDisplay extends StatelessWidget {
  final String textToDisplay;

  TextDisplay(this.textToDisplay);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(textToDisplay),
    );
  }
}