// 1) Create a new Flutter App (in this project) and output an AppBar and some text
// below it
// 2) Add a button which changes the text (to any other text of your choice)
// 3) Split the app into three widgets: App, TextControl & Text

import 'dart:math';

import 'package:assignment_one/TextDisplay.dart';
import 'package:assignment_one/button.dart';
import 'package:flutter/material.dart';

main() => {runApp(App())};

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final wordList = [
    'word',
    'anotherWord',
    'takeAWildBloddyGuess',
    'HereWeGoAgain'
  ];
  var _wordIndex = 0;
  void _changeWord() {
    setState(() {
      _wordIndex = Random().nextInt(wordList.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Random word Genrator"),
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            TextDisplay(wordList[_wordIndex]),
            buttonChange(_changeWord)
          ],
        )),
      ),
    );
  }
}
