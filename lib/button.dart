import 'package:flutter/material.dart';

class buttonChange extends StatelessWidget {
  final Function changeWordFunc;
  buttonChange(this.changeWordFunc);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        child: Text("change Word"),
        onPressed: changeWordFunc,
      ),
    );
  }
}
